#ifndef POSITION_H
#define POSITION_H

#include <string>
#include <ostream>

using namespace std;

class position{
    public:
        position( int In_Y = -1, int In_X = -1 );
        ~position();

        friend position operator+(position In_LeftHandSide, position In_RightHandSide);
        friend position operator-(position In_LeftHandSide, position In_RightHandSide);
        friend position operator*( position In_LeftHandSide, int In_Skalar );
        friend bool operator< ( const position& In_LeftHandSide, const position& In_RightHandSide );
        friend bool operator<= ( const position& In_LeftHandSide, const position& In_RightHandSide );
        friend bool operator> ( const position& In_LeftHandSide, const position& In_RightHandSide );
        friend bool operator== ( const position& In_LeftHandSide, position& In_RightHandSide );
        friend string operator+ ( const string& In_LeftHandSide, position& In_RightHandSide );
        friend string operator+ ( const string& In_LeftHandSide, const position& In_RightHandSide );
        friend ostream& operator<< ( ostream &out, position &In_Position );
        //position& operator=(const position &In_RightHandSide);
        friend bool operator!= (const position& In_LeftHandSide, const position& In_RightHandSide);

        int Y = -1;
        int X = -1;

    protected:

    private:
};

#endif // POSITION_H
