#ifndef MOVEREQUEST_H
#define MOVEREQUEST_H

#include <iostream>

#include "../direction.h"
#include "../request.h"

using namespace std;

class moveRequest: public request{

    public:
        moveRequest( direction In_Direction );
        ~moveRequest();
        direction getDirection();

        virtual void foobar(){ cout << "meoyawnn! moveRequest" << endl; }

    protected:

    private:
        direction _Direction;


};


#endif // MOVEREQUEST_H
