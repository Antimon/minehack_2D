#ifndef CREATEACCOUNTREQUEST_H
#define CREATEACCOUNTREQUEST_H

#include <iostream>
#include <string>

#include "../request.h"

using namespace std;

class createAccountRequest: public request{

    public:
        createAccountRequest( string In_Name, string In_Password );
        ~createAccountRequest();

        string getName();
        string getPassword();

        virtual void foobar(){ cout << "meoyawnn! createAccountRequest" << endl; }

    protected:

    private:
        string _Name;
        string _Password;


};


#endif // CREATEACCOUNTREQUEST_H
