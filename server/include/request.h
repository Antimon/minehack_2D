#ifndef EVENT_H
#define EVENT_H

using namespace std;

// choose which way to go for distinguish the request-types
#define USE_ENUM false
#define USE_TYPEID true     //seems to be the fastest.. more testing!!
#define USE_DYNAMICCAST false

#if (USE_ENUM && USE_TYPEID) || (USE_ENUM && USE_DYNAMICCAST) || (USE_TYPEID && USE_DYNAMICCAST)
zamp!
#endif


#if USE_ENUM
typedef enum{
    type_createAccountRequest = 0,
    type_moveRequest = 1,
    type_attackRequest = 2
}requestType;
#endif // USE_ENUM

class request{

    public:
        request(
                #if USE_ENUM
                requestType In_RequestType
                #endif // USE_ENUM
                );
        virtual ~request();

        virtual void foobar(){};    // it seems, i need at least one of these...

        #if USE_ENUM
        requestType getRequestType();
        #endif // USE_ENUM

    protected:

    private:
        #if USE_ENUM
        requestType _RequestType;
        #endif // USE_ENUM


};





#endif // EVENT_H
