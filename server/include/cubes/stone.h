#ifndef STONE_H
#define STONE_H

#include "../cube.h"

using namespace std;

class worldMap;

class view;

class stone: public cube{

    public:
        stone( view* In_View, worldMap* In_WorldMap );
        ~stone();

    protected:

    private:

};

#endif // STONE_H
