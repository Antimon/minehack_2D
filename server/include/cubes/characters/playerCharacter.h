#ifndef PLAYERCHARACTER_H
#define PLAYERCHARACTER_H


#include "../character.h"
#include "../../chatRoom.h"

using namespace std;



class playerCharacter: public character{

    public:
        playerCharacter( view* In_View, worldMap* In_WorldMap, client* In_Client, chatRoom* In_GlobalChatRoom );
        ~playerCharacter();
        //action processRequest( request In_Request );

    protected:
    private:
        client* _Client;    //the client represents the connection. and because that will be established, before the player entered his password, it will exist bevor the character-object
        //vector< playerCharacter* >* _AllPlayerCharacters;
        string _Password = "";
        string _Name = "";
        worldMap* _WorldMap;

        chatRoom* _GlobalChatRoom;
        //chatRoom* _WorldMapChatRoom;  // this is not necessary, because it will be a member of _WorldMap
        vector< chatRoom* > _OtherChatRooms;

};








#endif // PLAYERCHARACTER_H
