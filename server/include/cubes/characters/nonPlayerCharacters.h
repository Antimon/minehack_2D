#ifndef NONPLAYERCHARACTER_H
#define NONPLAYERCHARACTER_H

#include "../character.h"

using namespace std;

class nonPlayerCharacter: public character{

    public:
        nonPlayerCharacter( view* In_View, worldMap* In_WorldMap, bool In_IsAggressiv );
        ~nonPlayerCharacter();
    protected:
    private:
        bool _IsAggressiv = false;      // will attack as soon as it sees a playerCharacter

};








#endif // NONPLAYERCHARACTER_H
