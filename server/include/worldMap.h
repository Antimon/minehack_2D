#ifndef WORLDMAP_H
#define WORLDMAP_H

#include <map>
#include <mutex>

#include "./position.h"
#include "./head.h"
#include "./parameters.h"
#include "./chatRoom.h"
#include "./view.h"

using namespace std;

/*!
#include "./cubes/stone.h"
//class stone;
*/

class database;

class head;

class cube;

typedef enum{
    stone,
    water,
    sand,
    grass,
    tree
}cubeType;

class worldMap{

    public:
        worldMap( mapParameters* In_Parameters, view* In_View, database* In_Database, string In_Name );
        virtual ~worldMap();

        bool changePosition( const position In_OldPosition, const position In_NewPosition );

    protected:

    private:
        map< position, head* > _Map;
        view* _View = NULL;
        string _Name;
        database* _Database = NULL;
        mapParameters* _Parameters;
        chatRoom* _ChatRoom;
        mutex _Mutex;

        cubeType RandomSeedArray[100];

        void buildAsOpen();
        void buildEmptyMap();
        void print();
        void generateRandomSeedArray();
        cube* getRandomCubeSeed();

};







#endif // WORLDMAP_H
