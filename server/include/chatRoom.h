#ifndef CHAtROOM_H
#define CHAtROOM_H

#include <vector>
#include <mutex>

#include "./view.h"

using namespace std;

class playerCharacter;

class chatRoom{
    public:
        chatRoom( view* In_View, string In_Name );
        ~chatRoom();

        bool addPlayerCharacter( playerCharacter* In_PlayerCharacter );
        void removePlayerCharacter( playerCharacter* In_PlayerCharacter );

    protected:

    private:
        view* _View;
        vector< playerCharacter* > _Members;
        string _Name;
        mutex _Mutex;

};


#endif
