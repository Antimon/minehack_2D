#ifndef CONNECTIONMANAGER_H
#define CONNECTIONMANAGER_H

#include <sys/types.h>      //
#include <sys/socket.h>     //        = needed for the socket and server stuff
#include <netinet/in.h>     //
#include <vector>

#include "./parameters.h"
#include "./view.h"
#include "./client.h"

using namespace std;

class database;

class playerCharacterGenerator;

class connectionManager{


    public:
        connectionManager( globalParameters* In_Parameters, view* In_GlobalFunctions, database* In_Database, playerCharacterGenerator* In_PlayerCharacterGenerator/*!, vector<playerCharacter*>* In_AllPlayerCharacters*/ );
        ~connectionManager();

        void run();

    protected:

    private:

        bool initSocketCommunication();

        globalParameters* _Parameters = NULL;
        view* _View = NULL;
        database* _Database = NULL;
        playerCharacterGenerator* _PlayerCharacterGenerator = NULL;
        int64_t _SocketFileDescriptor = -1;
        int64_t _Port = -1;

        struct sockaddr_in _Server_addr;
        struct sockaddr_in _Client_addr;
        //A sockaddr_in is a structure containing an internet address. This structure is defined in netinet/in.h.
        //      struct sockaddr_in{
        //       short   sin_family; // must be AF_INET
        //       u_short sin_port;
        //       struct  in_addr sin_addr;
        //          char    sin_zero[8]; // Not used, must be zero
        //      };
        //An in_addr structure, defined in the same header file, contains only one field, a unsigned long called s_addr.
        //The variable serv_addr will contain the address of the server, and cli_addr will contain the address of the client which connects to the server.

        socklen_t _SizeOfAdressOfClient;

        vector<client*> _AllClients;




};




#endif // CONNECTIONMANAGER_H
