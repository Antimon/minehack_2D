#ifndef VIEW_H
#define VIEW_H

#include <string>
#include <mutex>

using namespace std;

typedef enum{
    tab_in = 0,
    tab_out = 1,
    tab_stay = 2
}tabbing;

class view{
    public:
        view();
        ~view();

        void output( tabbing In_Tabbing, string In_NameOfFunctionThisFunctionIsCalledFrom, string In_OutputString, bool In_ExitNow );
        void dump( string In_String );

        void writeInLogFile( /*Logfile In_WhichLogFile,*/ string In_ToWriteText );

    protected:

    private:
        mutex _OutputMutex;
        int64_t _TabLevel = 0;
        string getTimeInMilliseconds();
        string getDaytime();

};

#endif // VIEW_H
