#ifndef CUBE_H
#define CUBE_H

#include <string>

using namespace std;

/*!


//class worldMap;
*/

#include "./direction.h"

#include "./view.h"
//class view;

#include "./worldMap.h"
class worldMap;

#include "./position.h"
//class position;

class cube{


    public:
        cube( view* In_View, worldMap* In_WorldMap, bool In_IsMassive );
        virtual ~cube();
        int getEnergy();
        bool getIsMassive();

        bool processRequest(
                            #if USE_ENUM
                            requestType In_RequestType,
                            #endif // USE_ENUM
                            void* In_Request );


    protected:
        view* _View = NULL;
        bool _IsMassive = false;            // blocks sight?
        int _Energy = 100;                 //something from 0 to 100; it breaks, when it's 0
        worldMap* _worldMap;
        position _Position;

        bool moveToDirection( const direction In_Direction );   // gets called from processRequest()

    private:



};

#endif // CUBE_H
