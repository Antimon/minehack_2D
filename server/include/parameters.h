#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <string>

#include "../include/view.h"

using namespace std;

typedef enum{
    dungon,
    open
}mapType;


class globalParameters{

    public:
        globalParameters();
        ~globalParameters();

        friend ostream& operator<< ( ostream &out, globalParameters &In_Parameters );

        string _Password = "";
        bool _MakeEmptyWorld = false;
        bool _Heartbleed = false;
        bool _MakeNew = false;
        int64_t _AverageMapSize_Y = -1;
        int64_t _AverageMapSize_X = -1;
        float _DeltaSizeFactor = -1;
        int64_t _Port = -1;

    protected:

    private:


};


class mapParameters{

    public:
        mapParameters( globalParameters* In_GlobalParameters, view* In_View, mapType In_Type, int In_MaxY, int In_MaxX, float In_ProbabilityOfGettingASeed, float In_ProbabilityOfGettingStoneSeed, float _ProbabilityOfGettingWaterSeed, float _ProbabilityOfGettingTreeSeed, float _ProbabilityOfGettingSandSeed, float _ProbabilityOfGettingGrasSeed );
        ~mapParameters();

        globalParameters _GlobalParameters;
        mapType _Type;

        int64_t _MaxY = -1;
        int64_t _MaxX = -1;
        float _ProbabilityOfGettingASeed;

        float _ProbabilityOfGettingStoneSeed;
        float _ProbabilityOfGettingWaterSeed;
        float _ProbabilityOfGettingTreeSeed;
        float _ProbabilityOfGettingSandSeed;
        float _ProbabilityOfGettingGrasSeed;

    protected:

    private:
        view* _View;



};

#endif // PARAMETERS_H
