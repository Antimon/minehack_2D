#ifndef HEAD_H_INCLUDED
#define HEAD_H_INCLUDED

#include <vector>
#include <map>

using namespace std;

#include "./cube.h"
class cube;

class playerCharacter;

class client;


class head{
    public:
        head();
        ~head();

        vector<playerCharacter*> PlayerCharactersCurrentlySeeThisPosition;
        cube* _Cube = NULL;


        map< client*, bool > ClientsSawThisPositionInThisSession;   // client, bool = the cube has changed since the character saw this position last time

    protected:
    private:

};




#endif // HEAD_H_INCLUDED
