#ifndef DATABASE_H
#define DATABASE_H

#include <string>
#include <mutex>

using namespace std;

class cube;

#include "./view.h"

class database{
    public:
        database( view* In_View );
        ~database();

        bool writeCube( cube* In_Cube );

    protected:
    private:
        view* _View;
        mutex _Mutex;
};

#endif // DATABASE_H
