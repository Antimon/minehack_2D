#ifndef CLIENT_H
#define CLIENT_H

#include <iostream>
#include <vector>

using namespace std;

/*!

#include "./requests/moveRequest.h"
*/

#include "./requests/createAccountRequest.h"
//class createAccountRequest;

class request;

#include "./playerCharacterGenerator.h"
//class playerCharacterGenerator;

#include "./view.h"
//class view;

class playerCharacter;

class client{

    public:
        client(  view* In_View, int64_t In_SocketFileDescriptor, int64_t In_ClientID/*, database* In_Database*/, playerCharacterGenerator* In_PlayerCharacterGenerator );
        ~client();

        void run();

    protected:

    private:

        playerCharacter* _PlayerCharacter = NULL;
        view* _View = NULL;
        //database* _Database = NULL;
        playerCharacterGenerator* _PlayerCharacterGenerator = NULL;
        int64_t _ID = -1;
        int64_t _SocketFileDescriptor = -1;
        bool _Connected = false;

        string getNewMessageFromSocket();
        void markAsDissconnected();


        void extractPasswordAndCharacterName( const string In_PasswordAndCharacterName, string* Out_Password, string* Out_CharacterName);
        request* parseMessage( string In_Message );
        void processRequest( request* In_Request );
        void sendAcknowledgmentForNewAccountRequest();
        void sendMessage(string In_Message);

};




#endif // CLIENT_H
