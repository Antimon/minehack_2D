#ifndef CONFIGPARSER_H
#define CONFIGPARSER_H

#include <vector>

#include "parameters.h"

using namespace std;

class configParser{

    public:
        configParser();
        ~configParser();

        void readConfigs( globalParameters* In_Parameters, vector<string>* In_Lines );

    protected:

    private:
};




#endif // CONFIGPARSER_H
