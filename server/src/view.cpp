//!#include <time.h>
#include <sys/timeb.h>
#include <sys/time.h>           //needed for timestamp
#include <iostream>
#include <fstream>

#include "../include/view.h"



using namespace std;

view::view(){
    cout << "entering view::view()" << endl;

    cout << "leaving view::view()" << endl;
}

view::~view(){
    //dtor
}


//! use this function for standard-output
void view::output( tabbing In_Tabbing, string In_NameOfFunctionThisFunctionIsCalledFrom, string In_OutputString, bool In_ExitNow ){

    auto begin = In_NameOfFunctionThisFunctionIsCalledFrom.find_first_of("(") + 1;
    auto end = In_NameOfFunctionThisFunctionIsCalledFrom.find_last_of(")");
    if(string::npos!=begin && string::npos!=end && begin <= end){
        In_NameOfFunctionThisFunctionIsCalledFrom.erase(begin, end-begin);
    }else{
        cout << "_View-error ... Input:" << endl;
        cout << getDaytime() << getTimeInMilliseconds() << ": " << In_NameOfFunctionThisFunctionIsCalledFrom << In_OutputString << endl;
        cout << "exit!" << endl;
        exit(1);
    }


    if( In_Tabbing == tab_in ){
        _TabLevel++;
    }
    string Tabs = "";
    for( int64_t Index= 0; Index < _TabLevel; Index++ ){
        Tabs += "  ";
    }


    uint64_t StartSecondColumn = 80 - (In_NameOfFunctionThisFunctionIsCalledFrom.size() + Tabs.size());
    string Tabs_ToSecondColumn = "";

    for( uint64_t Index = 0; Index < StartSecondColumn; Index++ ){
        Tabs_ToSecondColumn += " ";
    }


    _OutputMutex.lock();

    cout << getDaytime() << getTimeInMilliseconds() << ": " << Tabs << In_NameOfFunctionThisFunctionIsCalledFrom << Tabs_ToSecondColumn << In_OutputString;
    if( In_ExitNow ){
        cout << " exit now!" << endl;
        exit(1);
    }
    cout << endl;

    if( In_Tabbing == tab_out ){
        _TabLevel--;
        if( _TabLevel < 0 ){
            cout << "tab-level below zero! exit now!" << endl;
            exit(1);
        }
    }

    _OutputMutex.unlock();


}

void view::dump( string In_String ){
    cout << In_String << endl;
}



string view::getTimeInMilliseconds(){
    struct timeval tp;
    gettimeofday(&tp, NULL);
    long int ms = tp.tv_sec * 1000 + tp.tv_usec / 1000;
    string TimeStamp = to_string(ms);
    //!TimeStamp.erase( 0, 4 );
    TimeStamp.erase( 0, 10 );
    return TimeStamp;
}


string view::getDaytime(){

    time_t     Now = time(0);
    struct tm  Tstruct;
    char       Buf[80];
    Tstruct = *localtime(&Now);
    // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
    // for more information about date/time format

    //!strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);
    strftime(Buf, sizeof(Buf), "%X", &Tstruct);

    return Buf;
}


void view::writeInLogFile( string In_ToWriteText ){



    timeb Tb;
    ftime (&Tb);
    fstream Fstream;                          //this writes the waitForMessageFromClient.log
    Fstream.open("generic.log", ios::out|ios::app );
    Fstream << "[" << getDaytime() << "]: " << In_ToWriteText << endl;
    Fstream.close();


}
