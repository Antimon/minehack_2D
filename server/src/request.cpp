#include "../include/request.h"

using namespace std;


request::request(
                 #if USE_ENUM
                 requestType In_RequestType
                 #endif // USE_ENUM
                  ){

    #if USE_ENUM
    _RequestType = In_RequestType;
    #endif // USE_ENUM

}


request::~request(){

}


#if USE_ENUM
requestType request::getRequestType(){
    return _RequestType;
}
#endif // USE_ENUM
