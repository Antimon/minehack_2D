#include <string>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <iterator>


#include "../include/configParser.h"


using namespace std;

configParser::configParser(){
    //ctor
}

configParser::~configParser(){
    //dtor
}


void configParser::readConfigs( globalParameters* In_Parameters, vector<string>* In_Lines ){

    for( size_t Index = 0; Index < In_Lines->size(); Index++ ){
        string Line = (*In_Lines)[Index];

        if( Line.size() != 0 ){ //if not empty line
            char First = Line.at(0);
            if( First != '#' ){         // not a comment
                istringstream StringStream(Line);
                vector<string> Words{
                    istream_iterator<string>{StringStream}, istream_iterator<string>{}
                };
                if( Words.size() != 2 ){
                    cout << "Line must be: name space value! exit now!" << endl;
                    exit(1);
                }else{
                    string Option = Words[0];
                    string Value = Words[1];

                    if( Option == "emptyWorld" ){
                        if( Value == "true" ){
                            cout << "setting MakeEmptyWorld to 'true'" << endl;
                            In_Parameters->_MakeEmptyWorld = true;
                        }else if( Value == "false" ){
                            cout << "setting MakeEmptyWorld to 'false'" << endl;
                            In_Parameters->_MakeEmptyWorld = false;
                        }else{
                            cout << "value of parameter '" << Option << "' must be 'true' or 'false' but is '" << Value << "' . exit now!" << endl;
                            exit(1);
                        }
                    }else if(Option == "password"){
                        if( Value.size() <= 16 ){
                            cout << "setting Password to '" << Value << "'" << endl;
                            In_Parameters->_Password = Value;
                        }else{
                            cout << "password in config-file ist longer then 16 characters. exit now!" << endl;
                            exit(1);
                        }

                    }else if( Option == "makeNew" ){
                        if( Value == "true" ){
                            cout << "setting Parameter 'makeNew' to 'true'" << endl;
                            In_Parameters->_MakeNew = true;
                        }else if( Value == "false" ){
                            cout << "setting Parameter 'makeNew' to 'false'" << endl;
                            In_Parameters->_MakeNew = false;
                        }else{
                            cout << "makeNew: invalid value '" << Value << "'. exit now!" << endl;
                            exit(1);
                        }

                    }else if(Option == "heartbleed"){
                        if( Value == "true" || Value == "false"  ){
                            cout << "setting heartbleed to '" << Value << "'" << endl;
                            if( Value == "true" ){
                                In_Parameters->_Heartbleed = true;
                            }else{
                                In_Parameters->_Heartbleed = false;
                            }
                        }else{
                            cout << "value of option 'heartbleed' must be 'true' or 'false' but is '" << Value << "'. exit now!" << endl;
                            exit(1);
                        }

                    }else if( Option == "averageMapSize_X" ){

                        if( ( In_Parameters->_AverageMapSize_X = stoi(Value) ) == false ){
                            cout << "error parsing _AverageMapSize_X from config-file. exit now!" << endl;
                            exit(1);
                        }

                    }else if( Option == "averageMapSize_Y" ){

                        if( ( In_Parameters->_AverageMapSize_Y = stoi(Value) ) == false ){
                            cout << "error parsing _AverageMapSize_Y from config-file. exit now!" << endl;
                            exit(1);
                        }

                    }else if( Option == "deltaSizeFactor" ){

                        if( ( In_Parameters->_DeltaSizeFactor = stof(Value) ) == false ){
                            cout << "error parsing deltaSizeFactor from config-file. exit now!" << endl;
                            exit(1);
                        }


                    }else if( Option == "port" ){

                        if( ( In_Parameters->_Port = stoi(Value) ) == false ){
                            cout << "error parsing port from config-file. exit now!" << endl;
                            exit(1);
                        }

                    }else{
                        cout << "unknown Line: " << Line << endl;
                        cout << "exit now!" << endl;
                        exit(1);
                    }
                }
            }else{
                cout << "Line is comment: " << Line << endl;
            }
        }
    }
}
