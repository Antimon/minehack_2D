#include "../include/parameters.h"

using namespace std;

globalParameters::globalParameters(){
    //ctor
}

globalParameters::~globalParameters(){
    //dtor
}


ostream& operator<< ( ostream &out, globalParameters &In_Parameters ){
    //! http://www.learncpp.com/cpp-tutorial/93-overloading-the-io-operators/



    // Since operator<< is a friend of the Point class, we can access
    // Point's members directly.
    out << ( "\tPassword: " + In_Parameters._Password + "\n" );

    if( In_Parameters._MakeEmptyWorld ){
        out << string( "\tMakeEmptyWorld: →true\n" );
    }else{
        out << string( "\tMakeEmptyWorld: →false\n" );
    }

    if( In_Parameters._Heartbleed ){
        out << string( "\tHeartbleed: →true\n" );
    }else{
        out << string( "\tHeartbleed: →false\n" );
    }

    if( In_Parameters._MakeNew ){
        out << string( "\tMakeNew: →true\n" );
    }else{
        out << string( "\tHeartbleed: →false\n" );
    }

    out << "\tAverageMapSize_X: " + to_string(In_Parameters._AverageMapSize_X) + "\n";
    out << "\tAverageMapSize_Y: " + to_string(In_Parameters._AverageMapSize_Y) + "\n";
    out << "\tdeltaSizeFactor: " + to_string(In_Parameters._DeltaSizeFactor) + "\n";
    out << ("\tPort: " + to_string(In_Parameters._Port) + "\n");

    //↓→øþ¨þ→↓←↓→ø


    return out;

}


mapParameters::mapParameters( globalParameters* In_GlobalParameters, view* In_View, mapType In_Type, int In_MaxY, int In_MaxX, float In_ProbabilityOfGettingASeed, float In_ProbabilityOfGettingStoneSeed, float In_ProbabilityOfGettingWaterSeed, float In_ProbabilityOfGettingTreeSeed, float In_ProbabilityOfGettingSandSeed, float In_ProbabilityOfGettingGrasSeed  ){
    _View = In_View;
    _View->output( tab_in, __PRETTY_FUNCTION__, "entering", false );
    _GlobalParameters = *In_GlobalParameters;
    _Type = In_Type;
    _MaxX = In_MaxX;
    _MaxY = In_MaxY;
    _ProbabilityOfGettingASeed = In_ProbabilityOfGettingASeed;
    _ProbabilityOfGettingStoneSeed = In_ProbabilityOfGettingStoneSeed;
    _ProbabilityOfGettingWaterSeed = In_ProbabilityOfGettingWaterSeed;
    _ProbabilityOfGettingTreeSeed = In_ProbabilityOfGettingTreeSeed;
    _ProbabilityOfGettingSandSeed = In_ProbabilityOfGettingSandSeed;
    _ProbabilityOfGettingGrasSeed = In_ProbabilityOfGettingGrasSeed;
    _View->output( tab_out, __PRETTY_FUNCTION__, "leaving", false );
}

mapParameters::~mapParameters(){

}


