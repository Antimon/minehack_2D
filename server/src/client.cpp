#include <unistd.h>     //usleep
#include <string.h>
#include <boost/algorithm/string.hpp>       //needed for splitting the received Messages in parseMessage()

#include "../include/client.h"


using namespace std;

client::client( view* In_View, int64_t In_SocketFileDescriptor, int64_t In_ClientID/*, database* In_Database*/, playerCharacterGenerator* In_PlayerCharacterGenerator ){
    _View = In_View;

    _ID = In_ClientID;
    _SocketFileDescriptor = In_SocketFileDescriptor;
    //_Database = In_Database;
    _PlayerCharacterGenerator = In_PlayerCharacterGenerator;

    _Connected = true;
}

client::~client(){
    //dtor
}



void client::run(){

    _View->output( tab_in, __PRETTY_FUNCTION__, "entering. ID: " + to_string(_ID), false );

    while( _Connected ){

        _View->output( tab_stay, __PRETTY_FUNCTION__, "waiting for new messgae from client " + to_string( _ID ), false );
        string NewMessage = getNewMessageFromSocket(  );

        if( _Connected ){   //we have to check again for connection here, because a not connected client just might be detected in getNewMessageFromSocket()

            request* NewRequest = parseMessage( NewMessage );

            processRequest( NewRequest );
            delete NewRequest;      //got allocated in parseMessage()

        }

    }

    _View->output( tab_out, __PRETTY_FUNCTION__, "leaving. ID: " + to_string(_ID), false );
}








string client::getNewMessageFromSocket( /*UserAccount *In_PointerUserAccount*/ ){           //get called by run()

    _View->output( tab_in, __PRETTY_FUNCTION__, "entering", false );

    string Output;

    int64_t SuccesfullyTransmitted = 0;                     //return value for the read() and write() calls; i.e. it contains the number of characters read or written.
    char ReceivedMessage_char[200000];                      //The server reads characters from the socket connection into this buffer.
    bzero( ReceivedMessage_char,200000 );                   //initializes the buffer using the bzero() function,
    char EmptyMessageForCompare[200000];                    //this is needed some lines below to check, if the reading was successful and
    strcpy(EmptyMessageForCompare, ReceivedMessage_char);   //init array


    if( _Connected ){

        _View->output( tab_stay, __PRETTY_FUNCTION__, "trying to read from client " + to_string(_ID) + " ; ClientIsConnected: " + to_string(_Connected) + " (1=true; 0=false)", false );

        try{
            SuccesfullyTransmitted = read( _SocketFileDescriptor,ReceivedMessage_char, 199999 );        // maybe deprecated, but...: got a BIG problem here. in this commit, the SocketFileDescriptor gets overwritten by something right after reading from the socket. but it seems, that this problem only accures when more then one client is connected... i guess it's because of unvalid pointer-handling of the character-stuff
        }catch(...){
            _View->output( tab_stay, __PRETTY_FUNCTION__, "catching at reading from Socket", false );
            markAsDissconnected();
        }
        //there is something for it to read in the socket, i.e. after
        //the client has executed a write(). It will read either the
        //total number of characters in the socket or 255, whichever
        //is less, and return the number of characters read. The read()
        //man page has more information.

        _View->output( tab_stay, __PRETTY_FUNCTION__, "Client[" + to_string(_ID) + "]: Successfullytransmitted: " + to_string(SuccesfullyTransmitted) + " ; ReceivedMessage: " + ReceivedMessage_char, false );
    }else{
        _View->output( tab_stay, __PRETTY_FUNCTION__, "Client[0" + to_string(_ID) + "]: offline", false );
    }



    _View->writeInLogFile( /*! waitForMessageFromClient, ??*/ ReceivedMessage_char );



    if( strcmp( EmptyMessageForCompare, ReceivedMessage_char ) == 0 && _Connected ){                          //i did this and the if(ClientIsConnected) above to finnally get rid of the problem, that if the first connected client disconnected made the server crash
        _View->output( tab_stay, __PRETTY_FUNCTION__, "reading an empty Message from Client " +  to_string(_ID), false);
        markAsDissconnected();
    }

    //Debug_online = ClientIsConnected;
    if(SuccesfullyTransmitted == 0 && _Connected ){
        markAsDissconnected();
        //cout << getTimestampInMiliseconds() << " Client "<< ClientID << ": waitForMsg(): Client " << ClientID << " disconnected. set ClientIsConnected = 'false'" << endl;
        _View->output( tab_stay, __PRETTY_FUNCTION__, "Client " + to_string( _ID ) + ": waitForMsg(): Client " + to_string( _ID ) + " disconnected. set ClientIsConnected = 'false'", false );
    }


    if (SuccesfullyTransmitted < 0 && _Connected){                                        // so i copy it to this place but without the exit(1)
        //cout << getTimestampInMiliseconds() << " client::waitForMsg(): ERROR reading from socket" << endl;
        _View->output( tab_stay, __PRETTY_FUNCTION__, "client::waitForMsg(): ERROR reading from socket", false );
        markAsDissconnected();
    }


    Output = string(ReceivedMessage_char);

    //cout << getTimestampInMiliseconds() << " leaving client::waitForMessageFromClient()" << endl;
    _View->output( tab_out, __PRETTY_FUNCTION__, "leaving", false );

    return Output;


}




void client::markAsDissconnected(){

    _View->output( tab_in, __PRETTY_FUNCTION__, "entering", false );

    _Connected = false;

    //!_Character->cleanUp();


    _View->output( tab_stay, __PRETTY_FUNCTION__, "Client " + to_string(_ID) + " . marked as dissconnected", false );

    _View->output( tab_out, __PRETTY_FUNCTION__, "leaving", false);
}


void client::extractPasswordAndCharacterName( const string In_PasswordAndCharacterName, string* Out_Password, string* Out_CharacterName){     //called from parseMessage()
    _View->output( tab_in, __PRETTY_FUNCTION__, "entering", false );
    //NewAccountRequests will have the format password*charactername

    int64_t SizeOfString = In_PasswordAndCharacterName.size();
    char Char_PasswordAndCharacterName[SizeOfString];
    strcpy(Char_PasswordAndCharacterName, In_PasswordAndCharacterName.c_str()); //copying incomming string in char-array
    bool PasswordComplete = false;
    char Password[32];
    char CharacterName[32];
    bzero(Password, 32);
    bzero(CharacterName, 32);
    int64_t CharacterNameIndex = 0;
    for ( int64_t StringIndex = 0; StringIndex < SizeOfString; StringIndex++ ){
        _View->output( tab_stay, __PRETTY_FUNCTION__, "StringIndex: " + to_string(StringIndex), false );

        if ( Char_PasswordAndCharacterName[StringIndex] != '*' && PasswordComplete == false ){
            Password[StringIndex] = Char_PasswordAndCharacterName[StringIndex];
        }
        if( PasswordComplete == false && Char_PasswordAndCharacterName[StringIndex] == '*' ){
            StringIndex++;
            PasswordComplete = true;
        }

        if( PasswordComplete == true && Char_PasswordAndCharacterName[StringIndex] != ';' ){
            CharacterName[CharacterNameIndex] = Char_PasswordAndCharacterName[StringIndex];
            CharacterNameIndex++;
        }


    }

    *Out_Password = Password;
    *Out_CharacterName = CharacterName;
    _View->output( tab_out, __PRETTY_FUNCTION__, "leaving", false );

}




request* client::parseMessage(string In_Message){        //gets called from client::run()


// for example: #CreateNewAccount;somePassword*someName;end

    _View->output( tab_in, __PRETTY_FUNCTION__, "entering. In_Message: " + In_Message, false );

    request* Output;

    vector<std::string> Fields;
    boost::split(Fields, In_Message, boost::is_any_of(";"));


    if( Fields.size() != 3 ){
        _View->output( tab_stay, __PRETTY_FUNCTION__, "received strange message, not according to our protocoll: " + In_Message, false );
    }else{

        string MessageType = Fields[0];
        string MessageValue = Fields[1];
        string EndSymbol = Fields[2];

        if( EndSymbol != "end" && EndSymbol != "end\n"){
//                                                 ^need that for netcat-experiments
            _View->output( tab_stay, __PRETTY_FUNCTION__, "wrong endsymbol. In_Message: " + In_Message + " ... exit now!", true );
        }else{

            string KeystringForChatMessage=                     "#ChatMessage-----";
            string KeystringForHeartbleedFromClient =           "#HeartbleedFromCl";
            string KeystringForRequestToMove =                  "#RequestToMove---";
            string KeystringForCreateNewCharacter =             "#CreateNuCharactr";
            string KeystringForLoginRequest =                   "#RequestToLogin--";
            string KeystringForClientWantsToCloseConnection =   "#CloseConnection-";
            string KeystringForPrimaryAttack =                  "#PrimaryAttack---";
            string KeystringForSecondaryAttack =                "#SecondaryAttack-";
            string KeystringForAdminCommand =                   "#AdminCommand----";

            if( MessageType == KeystringForChatMessage ){
                //missing
            }else if( MessageType == KeystringForHeartbleedFromClient ){
                //missing
            }else if( MessageType == KeystringForRequestToMove ){
                //missing
            }else if( MessageType == KeystringForCreateNewCharacter ){
                string Password;
                string CharacterName;
                extractPasswordAndCharacterName( MessageValue, &Password, &CharacterName );

                createAccountRequest* CreateAccountRequest = new createAccountRequest( CharacterName, Password ); // gets deleted in run()
                Output = static_cast<request*>(CreateAccountRequest);

            }else if( MessageType == KeystringForLoginRequest ){
                //missing
            }else if( MessageType == KeystringForClientWantsToCloseConnection ){
                //missing
            }else if( MessageType == KeystringForPrimaryAttack ){
                //missing
            }else if( MessageType == KeystringForSecondaryAttack ){
                //missing
            }else if( MessageType == KeystringForAdminCommand ){
                //missing
            }else{
                _View->output( tab_out, __PRETTY_FUNCTION__, "unknown Keystring. In_Message " + In_Message + " ... exit now!", true );
            }
        }
    }
    _View->output( tab_out, __PRETTY_FUNCTION__, "leaving", false );
    return Output;
}

void client::processRequest( request* In_Request ){     // gets called from client::run()
    _View->output( tab_in, __PRETTY_FUNCTION__, "entering", false );

    bool RequestDetected = false;

    #if USE_ENUM
    requestType RequestType = In_Request->getRequestType();
    #endif // USE_ENUM

    #if USE_DYNAMICCAST
    createAccountRequest* CreateAccountRequest_CastedPointer = NULL;
    CreateAccountRequest_CastedPointer = dynamic_cast<createAccountRequest*>(In_Request);
    #endif // USE_DYNAMICCAST

    if(
        #if USE_ENUM
        RequestType == type_createAccountRequest
        #endif // USE_ENUM

        #if USE_TYPEID
        typeid(*In_Request) == typeid(createAccountRequest)
        #endif // USE_TYPEID

        #if USE_DYNAMICCAST
        CreateAccountRequest_CastedPointer != NULL
        #endif // USE_DYNAMICCAST

        ){

        _View->output( tab_stay, __PRETTY_FUNCTION__, "detected createAccountRequest", false );
        RequestDetected = true;

        #if USE_ENUM || USE_TYPEID
        createAccountRequest* CreateAccountRequest_CastedPointer = static_cast<createAccountRequest*>(In_Request);
        #endif // USE_ENUM

        string Name = CreateAccountRequest_CastedPointer->getName();
        string Password = CreateAccountRequest_CastedPointer->getPassword();

        _PlayerCharacter = _PlayerCharacterGenerator->getNewPlayerCharacter( Name, Password, this );

        sendAcknowledgmentForNewAccountRequest();
    }


    #if USE_DYNAMICCAST
    moveRequest* MoveRequest_CastedPointer = NULL;
    MoveRequest_CastedPointer = dynamic_cast<moveRequest*>(In_Request);
    #endif // USE_DYNAMICCAST

    if(
       #if USE_ENUM
       RequestType == type_moveRequest
       #endif // USE_ENUM

       #if USE_TYPEID
       typeid(*In_Request) == typeid(moveRequest)
       #endif // USE_TYPEID

       #if USE_DYNAMICCAST
       MoveRequest_CastedPointer != NULL
       #endif // USE_DYNAMICCAST

       ){

        _View->output( tab_stay, __PRETTY_FUNCTION__, "detected MoveRequest", false );
        RequestDetected = true;
        moveRequest* MoveRequest_CastedPointer = NULL;

        #if USE_ENUM || USE_TYPEID
        MoveRequest_CastedPointer = static_cast<moveRequest*>(In_Request);
        #endif // USE_ENUM

        _PlayerCharacter->processMoveRequest( MoveRequest_CastedPointer );
    }

    //!more missing



    if( RequestDetected == false ){
        _View->output( tab_stay, __PRETTY_FUNCTION__,"request undetected!", true );
    }

    _View->output( tab_out, __PRETTY_FUNCTION__, "leaving", false );
}


void client::sendAcknowledgmentForNewAccountRequest(){
    _View->output( tab_in, __PRETTY_FUNCTION__, "entering", false );

    string ToSendString = "NewCharCreated--;";
    sendMessage( ToSendString );

   _View->output( tab_out, __PRETTY_FUNCTION__, "leaving", false );
}



void client::sendMessage(string In_Message){
    _View->output( tab_in, __PRETTY_FUNCTION__, "entering", false );
    In_Message = "#" + In_Message + ";end";
    char ToSendMessage_char[200000];
    bzero(ToSendMessage_char, 200000);
    strcpy( ToSendMessage_char, In_Message.c_str() );
    int64_t SuccessfullyWritten = -1;

    try{          //well lets try to catch the case, that the client has disconnected

        if(_Connected){                  //i guess i need to check that here again, because the status of the client can has changed in the meantime.
            SuccessfullyWritten = write(        //!Debug this is crashing when the first client is disconnecting
                                                _SocketFileDescriptor,
                                                ToSendMessage_char,
                                                strlen(ToSendMessage_char)
                                                                        );

            _View->output( tab_stay, __PRETTY_FUNCTION__, "SuccessfullyWritten = " + to_string( SuccessfullyWritten ), false  );
        }
    }catch(...){
        _View->output( tab_stay, __PRETTY_FUNCTION__, " Client " + to_string(_ID) + ": 'catch'ing writing error! ", false );
        markAsDissconnected();
    }

    if( SuccessfullyWritten < 0 && _Connected ){   //error-handling
        _View->output( tab_stay, __PRETTY_FUNCTION__, "ERROR writing to socket, somethin went veeeeerrrrrryyy wrong! ; variable 'SuccessfullyWritten' = " + to_string(SuccessfullyWritten), false );
        markAsDissconnected();
    }
    _View->output( tab_out, __PRETTY_FUNCTION__, "leaving", false );
}
