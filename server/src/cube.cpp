#include <stdlib.h>     // srand, rand
#include <sys/time.h>           //needed for timestamp

#include "../include/cube.h"

using namespace std;


cube::cube( view* In_View, worldMap* In_WorldMap, bool In_IsMassive ){
    _worldMap = In_WorldMap;
    _View = In_View;
    _IsMassive = In_IsMassive;
}


cube::~cube(){

}



int cube::getEnergy(){
    return _Energy;
}


bool cube::getIsMassive(){
    return _IsMassive;
}


bool cube::processRequest(
                          #if USE_ENUM
                          requestType In_RequestType,
                          #endif // USE_ENUM
                          void* In_Request ){

    _View->output( tab_in, __PRETTY_FUNCTION__, "entering", false );
    bool Output = false;



    _View->output( tab_out, __PRETTY_FUNCTION__, "leaving", false );
    return Output;
}

bool cube::moveToDirection( const direction In_Direction ){ // gets called from processRequest()
    _View->output( tab_in, __PRETTY_FUNCTION__, "entering", false );
    bool Output = false;

    position MoveVektor;
    if( In_Direction == direction::up ){
        MoveVektor.Y = -1;
    }else if( In_Direction == direction::down ){
        MoveVektor.Y = 1;
    }else if( In_Direction == direction::left ){
        MoveVektor.X = -1;
    }else if( In_Direction == direction::right ){
        MoveVektor.X = 1;
    }else if( In_Direction == direction::upleft ){
        MoveVektor.X = -1;
        MoveVektor.Y = -1;
    }else if( In_Direction == direction::upright ){
        MoveVektor.X = 1;
        MoveVektor.Y = -1;
    }else if( In_Direction == direction::downleft ){
        MoveVektor.X = -1;
        MoveVektor.Y = 1;
    }else if( In_Direction == direction::downright ){
        MoveVektor.X = 1;
        MoveVektor.Y = 1;
    }else{
        _View->output( tab_stay, __PRETTY_FUNCTION__, "something went wrong", true );
    }

    position NewPosition = _Position + MoveVektor;

    if( _worldMap->changePosition( _Position, NewPosition ) ){
        Output = true;
    }

    _View->output( tab_out, __PRETTY_FUNCTION__, "leaving", false );
    return Output;
}


