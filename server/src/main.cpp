/*

linker options:
-pthread

//-fsanitize=thread -fPIE -pie

compiler options:
//-std=gnu++11            //that doesn't seem to be needed any more!

-I include              //needed for cereal stuff

//-fsanitize=thread      //ThreadSanitizer for finding bugs and data-race-issues



//to run in valgrind, use: valgrind --log-file="valgrind.log" ./server

//to create analysis-file, use: gprof server gmon.out > analysis

*/


#include <iostream>
#include <vector>
#include <dirent.h>
#include <fstream>

#include "../include/parameters.h"
#include "../include/configParser.h"
#include "../include/view.h"
#include "../include/worldMap.h"
#include "../include/connectionManager.h"
#include "../include/database.h"
#include "../include/playerCharacterGenerator.h"


using namespace std;


int main( int argc, char* argv[] ){
    cout << "entering main" << endl;

    //system("clear");

    globalParameters GlobalParameters;

    cout << "argc: " << argc << endl;

    if( argc == 1 ){        //! no arguments provided

        //! using default parameters:

        GlobalParameters._Password =   "password--------";
        GlobalParameters._MakeEmptyWorld = false;
        GlobalParameters._Heartbleed = false;
        GlobalParameters._MakeNew = true;
        GlobalParameters._AverageMapSize_Y = 50;
        GlobalParameters._AverageMapSize_X = 80;
        GlobalParameters._DeltaSizeFactor = 0.4;
        GlobalParameters._Port = 51717;

    }else{
        for( int index = 1; index < argc; index++ ){
            //cout << "argc[" << index << "]: " << argv[index] << endl;
            string Temp_Argument = argv[index];



            if( Temp_Argument == "-h" || Temp_Argument == "--help" ){
                cout << "printing help..." << endl;
                cout << "hello, this is the server for minehack. minehack is an mmorpg in traditional rogue-like flavour." << endl;

                cout << "options:" << endl << endl;
                cout << "\e[1m" << "-h, --help" << "\e[0m" << endl;
                cout << "\tdisplay this information!" << endl;

                cout << endl << "\e[1m" << "-p, --password" << "\e[0m" << endl;
                cout << "\tset password. must be less then 16 characters!" << endl;

                cout << endl << "\e[1m" << "-mn, --makeNew" << "\e[0m" << endl;
                cout << "\toverwrite current database." << endl;

                cout << endl << "\e[1m" << "-e, --empty" << "\e[0m" << endl;
                cout << "\tcreate an empty world. that is quicker and enough for most test-cases." << endl;

                cout << endl << "\e[1m" << "-hb, --heartbleed" << "\e[0m" << endl;
                cout << "\tsending a heartbeed to client." << endl;

                cout << endl << "\e[1m" << "-dc, --defaultConfig" << "\e[0m" << endl;
                cout << "\toverwrites any existing config-file with default values" << endl;

                cout << endl << "\e[1m" << "-P, --port" << "\e[0m" << endl;
                cout << "\twhich port should the server use? (must be the same as client" << endl;


                cout << endl << "\e[1m" << "-c, --config" << "\e[0m" << endl;
                cout << "\tuse config-file. must be in same folder as this binary. other options can still be used!" << endl;
                cout << "\tvalid options an a config file are: " << endl;
                cout << "\t\t password <somePassword>" <<endl;
                cout << "\t\t makeNew <true/false>" << endl;
                cout << "\t\t flatWorld <true/false>" << endl;
                cout << "\t\t heartbleed <true/false>" << endl;
                cout << "\t\t port <somePortNumber>" <<endl;
                cout << "\t\t averageMapSize_X <somePositivNumber>" << endl;
                cout << "\t\t averageMapSize_Y <somePositivNumber>" << endl;
                cout << "\t\t deltaSizeFactor <someFloatBetween0and1>" << endl;

                //cout << endl << "\e[1m" << "-agloz, --averageGroundLevelOverZero" << "\e[0m" << endl;
                //cout << "\tthe average groundLevel over Zero. Must be between 0 and init_Z." << endl;

                cout << endl << "exit now!" << endl;
                exit(0);


            //!this must stay at the beginning, because it must be able to be overwritten by other options!
            }else if( Temp_Argument == "-c" || Temp_Argument == "--config" ){
                //!checking if there is a "config"-file

                vector<string> FilesInThisFolder;
                DIR *dp;
                struct dirent *dirp;
                string ThisFolder = ".";
                if((dp  = opendir( ThisFolder.c_str() )) == NULL) {
                    cout << "Error(" << errno << ") opening " << "." << endl;
                    return errno;
                }

                while ((dirp = readdir(dp)) != NULL) {
                    (FilesInThisFolder).push_back(string(dirp->d_name));
                }
                closedir(dp);

                bool ConfigFound = false;
                for( size_t index = 0; index < FilesInThisFolder.size(); index++ ){
                    if( FilesInThisFolder[index] == "config" ){
                        ConfigFound = true;
                    }
                }

                vector<string> Lines;
                if( ConfigFound == false ){
                    cout << "no config-file found. exit now!" << endl;
                    exit(1);
                }else{
                    string Line = "";
                    ifstream ConfigFile("config");
                    while( getline(ConfigFile, Line) ){
                        Lines.push_back(Line);
                    }
                }
                configParser Parser;
                Parser.readConfigs(&GlobalParameters, &Lines);

            }else if( Temp_Argument == "-dc" || Temp_Argument == "--defaultConfig" ){
                cout << "do you really want to overwrite any existing config file? (y/n)" << endl;
                char Answer = getchar();
                if( Answer == 'y' || Answer == 'Y' ){
                    system("rm -r config"); //well, it might be a folder...
                    system("touch config");
                    system("echo 'password password' >> config");
                    system("echo 'makeNew true' >> config");
                    system("echo 'emptyWorld false' >> config");
                    system("echo 'heartbleed false' >> config");
                    system("echo 'port 51717' >> config");
                    system("echo 'averageMapSize_X 50' >> config");
                    system("echo 'averageMapSize_Y 80' >> config");
                    system("echo 'deltaSizeFactor 0.4' >> config");
                    system("echo '#some comment' >> config");

                    cout << "default-config generated. exit now! re-run server with '-c'-option to use the new config-file!" << endl;
                    exit(0);
                }else if( Answer == 'n' || Answer == 'N' ){
                    cout << "you typed '" << to_string(Answer) << "'. exit now!" <<endl;
                    exit(1);
                }else{
                    cout << "invalid answer. exit now!" << endl;
                    exit(1);
                }

            }else if( Temp_Argument == "-f" || Temp_Argument == "--flat" ){
                cout << "making a flat world" << endl;
                GlobalParameters._MakeEmptyWorld = true;
            }else if( Temp_Argument == "-hb" || Temp_Argument == "--heartbleed" ){
                cout << "turning heartbleed on" << endl;
                GlobalParameters._Heartbleed = true;
            }else if( Temp_Argument == "-p" || Temp_Argument == "--password" ){
                if( index+1 == argc ){
                    cout << "wrong usage. after '-p' or '--password' you must specify a password" << endl;
                }else{
                    string NextArgument = argv[index+1];
                    char FirstLetter = NextArgument.at(0);
                    if( FirstLetter == '-' ){
                        cout << "error. after '-p' or '--password' you must specify a password. exit now!" << endl;
                        exit(1);
                    }
                    GlobalParameters._Password = argv[index+1];
                    if( GlobalParameters._Password.size() > 16 ){
                        cout << "choosen password is to long. cant be more then 16 characters. exit now! " << endl;
                        exit(1);
                    }

                    for ( int I = GlobalParameters._Password.size() ; GlobalParameters._Password.size() < 16 ; I++ ){
                        GlobalParameters._Password += "-";
                    }
                    cout << "set password to: " << GlobalParameters._Password << endl;
                    index++;
                }

            }else if( Temp_Argument == "-mn" || Temp_Argument == "--makeNew" ){
                cout << "are you sure you want to delete the current database? (y/n)" << endl;
                char Answer = getchar();
                if( Answer == 121 || Answer == 89 ){        // ascii y or Y
                    GlobalParameters._MakeNew = true;
                }else if( Answer == 110 || Answer == 78 ){  //asscii n or N
                    cout << "you decided not to delete the database. exit now" << endl;
                    exit(0);
                }else{
                    cout << "wrong letter. exit now" << endl;
                    exit(1);
                }

            }else{
                cout << "unknown option '" << argv[index] << "'. exit now!" << endl;
                exit(1);
            }
        }
    }
    cout << "end parsing argumens: \n" << GlobalParameters << endl;

    //! buildung the objects to run the server
    view View;
    mapParameters* Init_MapParameters = new mapParameters( &GlobalParameters, &View, open, 30, 40, 0.2, 0.001, 0.02, 0, 0, 0 );
    database Database( &View );

    worldMap Init_WorldMap( Init_MapParameters, &View, &Database, "InitMap" );

    chatRoom GlobalChatroom( &View, "global" );

    vector< playerCharacter* > AllPlayerCharacters;
    playerCharacterGenerator PlayerCharacterGenerator( &View, &Database, &Init_WorldMap, &AllPlayerCharacters, &GlobalChatroom );



    connectionManager ConnectionManager( &GlobalParameters, &View, &Database, &PlayerCharacterGenerator );

    ConnectionManager.run();//blocking!

    //!cleaning up:
    delete Init_MapParameters;


    return 0;

}





