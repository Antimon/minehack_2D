#include "../include/position.h"

using namespace std;

position::position( int In_Y, int In_X ){

    Y = In_Y;
    X = In_X;

}

position::~position(){

}



position operator+(position In_LeftHandSide, position In_RightHandSide){

    In_LeftHandSide.Y += In_RightHandSide.Y;
    In_LeftHandSide.X += In_RightHandSide.X;

    return In_LeftHandSide;
}


position operator-(position In_LeftHandSide, position In_RightHandSide){

    In_LeftHandSide.Y -= In_RightHandSide.Y;
    In_LeftHandSide.X -= In_RightHandSide.X;

    return In_LeftHandSide;
}

position operator*( position In_LeftHandSide, int In_Skalar ){

    In_LeftHandSide.Y = In_LeftHandSide.Y * In_Skalar;
    In_LeftHandSide.X = In_LeftHandSide.X * In_Skalar;

    return In_LeftHandSide;
}


bool operator< ( const position& In_LeftHandSide, const position& In_RightHandSide ){

    if( In_LeftHandSide.Y < In_RightHandSide.Y ){
        return true;
    }else if( In_LeftHandSide.Y > In_RightHandSide.Y ){
        return false;
    }else if( In_LeftHandSide.X < In_RightHandSide.X ){
        return true;
    }else if( In_LeftHandSide.X > In_RightHandSide.X ){
        return false;
    }



    else{
        //!if the two sides are equal in all three dimensions, it just doesn't matter which one will be sorted first
        //I try i with returning false now, but it might be faster with true
        //!so i should benchmark that later!!!
        // changing it to "true" makes segmentation-faults
        return false;
    }
}



bool operator<= ( const position& In_LeftHandSide, const position& In_RightHandSide ){
    bool Output = false;
    //if( In_LeftHandSide.Z <= In_RightHandSide.Z ){
        if( In_LeftHandSide.X <= In_RightHandSide.X ){
            if( In_LeftHandSide.Y <= In_RightHandSide.Y ){
                Output = true;
            }
        }
    //}
    return Output;
}


bool operator> ( const position& In_LeftHandSide, const position& In_RightHandSide ){

    //!dirty-copy-pasted from above!

    if( In_LeftHandSide.Y > In_RightHandSide.Y ){
        return true;
    }else if( In_LeftHandSide.Y < In_RightHandSide.Y ){
        return false;
    }else if( In_LeftHandSide.X > In_RightHandSide.X ){
        return true;
    }else if( In_LeftHandSide.X < In_RightHandSide.X ){
        return false;
    }/*else if( In_LeftHandSide.Z > In_RightHandSide.Z ){
        return true;
    }else if( In_LeftHandSide.Z < In_RightHandSide.Z ){
        return false;
    }*/else{
        return false;
    }
}

bool operator== ( const position& In_LeftHandSide, position& In_RightHandSide ){
    bool Output = false;
    if( In_LeftHandSide.Y == In_RightHandSide.Y && In_LeftHandSide.X == In_RightHandSide.X /*&& In_LeftHandSide.Z == In_RightHandSide.Z*/ ){
        Output = true;
    }
    return Output;
}


string operator+ ( const string& In_LeftHandSide, position& In_RightHandSide ){
    string Output = "";
    Output = In_LeftHandSide + to_string(In_RightHandSide.Y) + ";" + to_string(In_RightHandSide.X) /*+ ";" + to_string(In_RightHandSide.Z)*/;
    return Output;
}

string operator+ ( const string& In_LeftHandSide, const position& In_RightHandSide ){
    string Output = "";
    Output = In_LeftHandSide + to_string(In_RightHandSide.Y) + ";" + to_string(In_RightHandSide.X) /*+ ";" + to_string(In_RightHandSide.Z)*/;
    return Output;
}



ostream& operator<< ( ostream &out, position &In_Position ){
    //! http://www.learncpp.com/cpp-tutorial/93-overloading-the-io-operators/

    // Since operator<< is a friend of the Point class, we can access
    // Point's members directly.
    out << In_Position.Y << ";" << In_Position.X /*<< ";" << In_Position.Z*/;
    return out;

}


/*
position& position::operator= (  position& In_RightHandSide ){

    position LeftHandSide( In_RightHandSide.Y, In_RightHandSide.X, In_RightHandSide.Z );

    return LeftHandSide;

    //return In_RightHandSide;

}
*/

/*
position& position::operator=(const position &In_RightHandSide) {
    this->Y = In_RightHandSide.Y;
    this->X = In_RightHandSide.X;
    this->Z = In_RightHandSide.Z;

    return *this;  // Return a reference to myself.
}
*/



bool operator!= (const position& In_LeftHandSide, const position& In_RightHandSide){
    bool Output = false;
    if( (In_LeftHandSide.Y != In_RightHandSide.Y) || (In_LeftHandSide.X != In_RightHandSide.X) /*|| (In_LeftHandSide.Z != In_RightHandSide.Z)*/ ){
        Output = true;
    }
    return Output;
}
