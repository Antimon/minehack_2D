

#include "../include/playerCharacterGenerator.h"

playerCharacterGenerator::playerCharacterGenerator( view* In_View, database* In_Database, worldMap* In_EntrenceMap, vector< playerCharacter* >* In_AllPlayerCharacters, chatRoom* In_GlobalChatRoom ){
    _Database = In_Database;
    _View = In_View;
    _EntrenceMap = In_EntrenceMap;
    _AllPlayerCharacters = In_AllPlayerCharacters;
    _GlobalChatroom = In_GlobalChatRoom;
}


playerCharacterGenerator::~playerCharacterGenerator(){

}


playerCharacter* playerCharacterGenerator::getNewPlayerCharacter( string In_Name, string In_Password, client* In_Client ){

    playerCharacter* NewCharacter = new playerCharacter( _View, _EntrenceMap, In_Client, _GlobalChatroom );
    //!playerCharacter NewCharacter( _View, _EntrenceMap, _AllPlayerCharacters, In_Client );

    _Database->writeCube(NewCharacter);

    return NewCharacter;

}

