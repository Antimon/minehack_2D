#include "../../include/requests/moveRequest.h"

using namespace std;

moveRequest::moveRequest( direction In_Direction )
#if USE_ENUM
: request(type_moveRequest)
#endif // USE_ENUM
{
    _Direction = In_Direction;
}

moveRequest::~moveRequest(){

}



direction moveRequest::getDirection(){
    return _Direction;
}
