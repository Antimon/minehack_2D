

from threading import Thread

#import view
import server
#import character
#import cube
#import audio
import subprocess

import json
import array

import os, platform
import curses
import time     #needed for sleep()
#from thread import start_new_thread
from threading import Thread
from random import randint
import math


class controller:

    
    

    def __init__(self, In_DisplayIntroScreen):                #konstruktor

        self.DisplayIntroScreen = In_DisplayIntroScreen
        
        self.Screen=0
        self.ClientIsConnected=0
        self.Server=0
        self.View=0

        self.Character=0
        self.LoggedIn = False

        self.DoPing = True
        self.TimeBetweenPingRequests = 2     #seconds between ping requests
        self.Ping = -1                       #saves ping-value to server

        self.Temp_HeightOfWorld = -1
        self.Temp_WidthOfWorld = -1
        self.Temp_SightRatio = -1
        self.Temp_MaxLevelOverZero = -1

        self.Sight_Ratio = 0     #the ratio between the sight radius in z-direction and the sight-radius in x/y-direction. will be send from server with login/account-creation-message and send stored here

        self.RandomMovementMode = False
        self.OutstandingRandomSteps = 0
        self.KillServerAfterRandomMoves = False      #only workes with default server-password 'password'

        self.GoDownLeftForSomeSteps_Mode = False
        self.OutstandingStepsFor_GoDownLeftForSomeSteps_Mode = 0
        
        self.ClientIsConnected = False
        self.Screen = curses.initscr()
        self.Server = server.server()
        
        self.TargetID = -1            #id of account in target
        


    def welcomeScreen(self):
        curses.curs_set(0)
        if self.DisplayIntroScreen:
            PosY = int((self.Screen.getmaxyx()[0])/2) -3
            PosX = int((self.Screen.getmaxyx()[1])/2) - 40
            try:
                self.Screen.addstr(PosY  , PosX, "███╗   ███╗██╗███╗   ██╗███████╗██╗  ██╗ █████╗  ██████╗██╗  ██╗ ")
                self.Screen.addstr(PosY+1, PosX, "████╗ ████║██║████╗  ██║██╔════╝██║  ██║██╔══██╗██╔════╝██║ ██╔╝ ")
                self.Screen.addstr(PosY+2, PosX, "██╔████╔██║██║██╔██╗ ██║█████╗  ███████║███████║██║     █████╔╝  ")
                self.Screen.addstr(PosY+3, PosX, "██║╚██╔╝██║██║██║╚██╗██║██╔══╝  ██╔══██║██╔══██║██║     ██╔═██╗  ")
                self.Screen.addstr(PosY+4, PosX, "██║ ╚═╝ ██║██║██║ ╚████║███████╗██║  ██║██║  ██║╚██████╗██║  ██╗ ")
                self.Screen.addstr(PosY+5, PosX, "╚═╝     ╚═╝╚═╝╚═╝  ╚═══╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝ ")
                self.Screen.refresh()
                time.sleep(2)
                self.Screen.addstr(PosY-1, PosX+65, " .d8888b.  8888888b.  ")
                self.Screen.addstr(PosY  , PosX+65, 'd88P  Y88b 888  "Y88b ')
                self.Screen.addstr(PosY+1, PosX+65, "       888 888    888 ")
                self.Screen.addstr(PosY+2, PosX+65, "     .d88P 888    888 ")
                self.Screen.addstr(PosY+3, PosX+65, ' .od888P"  888    888 ')
                self.Screen.addstr(PosY+4, PosX+65, 'd88P"      888    888 ')
                self.Screen.addstr(PosY+5, PosX+65, '888"       888  .d88P ')
                self.Screen.addstr(PosY+6, PosX+65, '888888888  8888888P"  ')
                self.Screen.addstr(PosY+7, PosX+65, "")
                self.Screen.refresh()
                time.sleep(1.6)
                self.Screen.addstr(PosY+6, PosX, "Now")
                self.Screen.refresh()
                time.sleep(0.4)
                self.Screen.addstr(PosY+6, PosX+4, "with")
                self.Screen.refresh()
                time.sleep(0.4)
                self.Screen.addstr(PosY+6, PosX+9, "less")
                self.Screen.refresh()
                time.sleep(0.4)
                self.Screen.addstr(PosY+6, PosX+14, "Dimensions!")
                self.Screen.refresh()
                time.sleep(3)
            except:
                self.debug("skipping 'ASCII-Art-Logo' terminal to small?")
                PosY = int((self.Screen.getmaxyx()[0])/2) - 1
                PosX = int((self.Screen.getmaxyx()[1])/2) - 24
                time.sleep(2)
                self.Screen.addstr(PosY, PosX, "minehack 2D - Now! On your damn small Terminal!")
                self.Screen.refresh()
                time.sleep(3)

        #curses.nonl()
        time.sleep(1)
        self.Screen.erase()
        self.Screen.refresh()
        
        Charactername = "blank"
        EnergyOfCharacter = -1
        curses.noecho()
        curses.cbreak()                        #Applications will also commonly need to react to keys instantly, without requiring the Enter key to be pressed; this is called cbreak mode, as opposed to the usual buffered input mode.
        self.Screen.keypad(1)
        self.Screen.addstr(0,0, "this is MineHack 2D")
        self.Screen.addstr(1,0, "world's second virtual-reality-rogue-like-massivly-multiplayer-online-role-playing-game (VRRLMMORPG)")
        self.Screen.addstr(2,0, "but the first in two dimensions")
        self.Screen.addstr(3,0, "press any key to connect to server")
        self.Screen.getch()
        self.Screen.addstr(4,0, "...connecting...")
        self.ClientIsConnected=self.Server.connectToServer()
        
        if ( self.ClientIsConnected == True ):
            self.Screen.addstr(5,0, "welcome to doomsday-device.de, sir!")
            self.Screen.addstr(6,0, "press 1 to log in with an existing account, press 2 to creat a new one")
            Input = self.Screen.getch()
            if Input == 49:         #ascii for 1
                pass
                #missing: login with existing account
            elif Input == 50:         #ascii for 2
                self.Screen.addstr(7,0, "you pressed 2. lets create a new account!")
                curses.echo()
                self.Screen.addstr( 12,0, "please choose character-name." )
                Charactername = self.Screen.getstr(13, 0, 32)
                self.Screen.addstr(16,0, "type in your password. this will be transmitted and stored in plain text (because i'm lazy)\non the server, so don't use your common one here! (max. 32 characters, no special characters)")
                Password=self.Screen.getstr(18,0, 32)
                self.Server.sendStringToServer( "#CreateNuCharactr",Password.decode('utf-8') + "*" + Charactername.decode('utf-8') )
                self.Screen.refresh()
                ReceivedMessage=self.Server.readFromSocket()
                MessageType = ReceivedMessage.split("#", 1)[1].split(";",3)[0]
                MessageContent = ReceivedMessage.split("#", 1)[1].split(";",3)[1]
                if MessageType == "NewCharCreated--":
                    self.Screen.addstr(21,0, "new chartacter created!")
                    self.Screen.refresh()
                    self.LoggedIn = True
                    EnergyOfCharacter = MessageContent.split("*")[3]
                    self.Temp_SightRatio = float( MessageContent.split("*")[4].split(":")[1] )
                    self.Temp_HeightOfWorld = int( MessageContent.split("*")[5].split(":")[1] )
                    self.Temp_WidthOfWorld = int( MessageContent.split("*")[6].split(":")[1] )
                    self.Temp_MaxLevelOverZero = int( MessageContent.split("*")[7].split(":")[1] )
                if MessageType == "NameAlreadyInUse":
                    self.Screen.addstr(21,0, "can't create new account. '" + str( Charactername ) + "' is already in use!")
                    self.Screen.refresh()
            else:
                self.shutdownGame()
                print("didn't press 1 or 2")
                self.Screen.refresh()
        else:
            self.Screen.addstr(3,0, "seems to be no doomsday today! server offline :-(")
            self.Screen.getch()
        time.sleep(2)
        self.Character=character.character()
        self.Character.Name = str(Charactername)
        self.Character.Energy = int(EnergyOfCharacter)
        
    def enterMaingame(self):
        self.welcomeScreen()
        #that is annoying when i work on the view. but i need to fake it:
        #self.ClientIsConnected = True
        if self.LoggedIn == True:
            self.buildView()
        else:
            self.shutdownGame()

    def getTimeInMilliseconds(self):
        return time.time()


    def beep(self):
        os.system("bash -c \"speaker-test -t sine -f 440 -l 1 -p 2 >/dev/null\"")
        
        

    def debug(self, In):
        ToWriteMessage = "[" + str(time.time()) + "]: " + str(In) + "\n"
        with open("log/debug.log", 'a') as out:
            out.write( ToWriteMessage )
            
            
