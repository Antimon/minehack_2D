
from threading import Thread
import socket
import os, platform

import time

class server:


    def __init__(self):
        
        self.ServerAddress="127.0.0.1"            #localhost
        #self.ServerAddress="doomsday-device.de"    #from outside
        
        self.PortNumber = 51717
        self.Socket=0
        self.ToSendMessages=0
        self.ServerStillOnline=False

    def getServerAddress(self):
        return self.ServerAddress

    def connectToServer(self):
        Output=False
        self.Socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.Socket.connect((self.ServerAddress, self.PortNumber))
            Output=True
            ServerStillOnline=True
        except socket.error:
            Output=False
        return Output


    def sendStringToServer ( self, In_TypeOfMessage, In_ToSendMessage ):
        self.debug("entering sendStringToServer")
        pass
        ToSendMessage= In_TypeOfMessage + ";" + In_ToSendMessage + ";end"
        pass
        #ToSendMessage = bytes(ToSendMessage, "encoding")
        ToSendMessage = str.encode(ToSendMessage)
        pass
    #    self.debug("ToSendMessage befor writing socket:")
    #    self.debug( ToSendMessage )
        try:
            self.Socket.send( ToSendMessage )
        except IOError:
            self.debug("sendStringToServer(): server seems offline!")
            print("server seems offline!. exit now!")
            exit()
        pass
        ToWriteMessage = "[" + str(time.time()) + "]: " + str(ToSendMessage) + "\n"
        with open('log/sendStringToServer.log', 'a') as out:
            out.write( ToWriteMessage )

    def closeConnection(self):
        self.debug("entering closeConnection()")
        try:
            self.Socket.shutdown(socket.SHUT_RDWR)
            self.beep()
        except socket.error:
            pass

    def readFromSocket(self):            #called from controller.waitForIncommingMessages()
        self.debug("entering readFromSocket()")
        Output = ""
        EndSymbol = ""
        while (str(EndSymbol) != ";end"):
            pass
            Received = b""
            try:
                Received = self.Socket.recv(200000)
            except IOError:                        #this fixes the window-resize error
                pass
                self.debug("IOError in readFromSocket")
                return "TerminalResized"
            Received_Decoded = Received.decode( 'utf-8' )
            pass
            #self.debug("Received: " + str( Received ) )
            if Received == b'':                            #this solves the problem of a crazy-cpu usage after server is offline
                pass
                self.debug("readFromSocket(): server seems offline")
                print("readFromSocket(): server seems offline")
                return "ServerOffline"
            pass
            Output = Output + Received_Decoded
            EndSymbol = Output[-4:]
        pass
        #this writes the readFromSocket.log
        ToWriteMessage = "[" + str(time.time()) + "]: " + str(Output) + "\n"
        with open("log/readFromSocket.log", 'a') as out:
            out.write( ToWriteMessage )
        
        
        if Output == "":
            #exit()
            #self.beep()
            Output = "#ServerOffline---;foo;end"
            self.debug("server offline")
        return Output

    def beep(self):
        os.system("bash -c \"speaker-test -t sine -f 440 -l 1 -p 2 >/dev/null\"")


    def debug(self, In):
        ToWriteMessage = "[" + str(time.time()) + "]: " + str(In) + "\n"
        with open("log/debug.log", 'a') as out:
            out.write( ToWriteMessage )

